//
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

const $ = require('jquery');
const d3 = require('d3');
const React = require('react');

const SamplePieChartPanel = React.createClass({
    render: function () {
      return (
        <svg id="piechartsvg"></svg>
      );
    },
    buildPieChart(){
      var dataset = [
  			{legend:"apple", value:10, color:"red"},
  			{legend:"orange", value:45, color:"orangered"},
  			{legend:"banana", value:25, color:"yellow"},
  			{legend:"peach", value:70, color:"pink"},
  			{legend:"grape", value:20, color:"purple"}
  			];

  		var width = 350;
  		var height = 200;
  		var radius = 100;

  		var arc = d3.svg.arc()
  				.outerRadius(radius)
  				.innerRadius(30);

  		var piechartsvg = d3.select("#piechartsvg")
  			.attr("width", width)
  			.attr("height", height)
  			.append("g")
  			.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


  		var pie = d3.layout.pie()
  				.sort(null)
  				.value(function(d){ return d.value; });

  		var g = piechartsvg.selectAll(".fan")
  				.data(pie(dataset))
  				.enter()
  				.append("g")
  				.attr("class", "fan")

  		g.append("path")
  			.attr("d", arc)
  			.attr("fill", function(d){ return d.data.color; })

  		g.append("text")
  			.attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
  			.style("text-anchor", "middle")
  			.text(function(d) { return d.data.legend; });
    },
    componentDidMount(){
      console.log("buildLineChart component did mount");
      this.buildPieChart();
    },
    componentWillMount(){
      console.log("buildLineChart component will mount");
    },
    componentWillUnmount(){
      console.log("Line unmount");
    }
});

module.exports = SamplePieChartPanel;
