//
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

const React = require('react');
const ReactDOM = require('react-dom');

// Build and Render Toolbar
const DateInput = require('../../js/components/DateInput.react');
const MainMenu = require('../../js/menu/components/MainMenu.react');

ReactDOM.render(
  <MainMenu />,
  document.getElementById('main-menu')
);

ReactDOM.render(
  (
  <form className="form-inline">
    <div className="form-group">
      <label htmlFor="dataDatePicker" className="control-label">Data Date:</label>
      <div className="input-group input-group-xs">
        <DateInput id="dataDatePicker"/>
      </div>
    </div>
  </form>
 ),
  document.getElementById('nav_form')
);

// Build and Render Edge Investigation's panels
const PanelRow = require('../../js/components/PanelRow.react');
const Panel = require('../../js/components/Panel.react');

const SampleBarChartPanel = require('./components/SampleBarChartPanel.react');
const SampleLineChartPanel = require('./components/SampleLineChartPanel.react');
const SamplePieChartPanel = require('./components/SamplePieChartPanel.react');
const SampleSideBarPanel = require('./components/SampleSideBarPanel.react');

ReactDOM.render(

  <div id="spot-content">
    <PanelRow>
      <Panel title="Line Chart" container expandable >
       <SampleLineChartPanel/>
      </Panel>
      <Panel title="Pie Chart" container expandable >
       <SamplePieChartPanel/>
      </Panel>
      <Panel title="Bar Chart" container expandable >
       <SampleBarChartPanel/>
      </Panel>
    </PanelRow>
  </div>,
  document.getElementById('spot-content-wrapper')
);
