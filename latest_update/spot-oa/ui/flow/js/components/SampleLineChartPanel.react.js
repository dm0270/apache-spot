//
// Licensed to the Apache Software Foundation (ASF) under one or more
// contributor license agreements.  See the NOTICE file distributed with
// this work for additional information regarding copyright ownership.
// The ASF licenses this file to You under the Apache License, Version 2.0
// (the "License"); you may not use this file except in compliance with
// the License.  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

const $ = require('jquery');
const d3 = require('d3');
const React = require('react');

const SampleLineChartPanel = React.createClass({
    render: function () {
      return (
        <svg id="linechartsvg"></svg>
      );
    },
    buildLineChart(){
            // Set the dimensions of the canvas / graph
      var margin = {top: 30, right: 20, bottom: 30, left: 50},
          width = 400 - margin.left - margin.right,
          height = 150 - margin.top - margin.bottom;

      // Parse the date / time
      var parseDate = d3.time.format("%d-%b-%y").parse;

      // Set the ranges
      var x = d3.time.scale().range([0, width]);
      var y = d3.scale.linear().range([height, 0]);

      // Define the axes
      var xAxis = d3.svg.axis().scale(x)
          .orient("bottom").ticks(5);

      var yAxis = d3.svg.axis().scale(y)
          .orient("left").ticks(5);

      // Define the line
      var valueline = d3.svg.line()
          .x(function(d) { return x(d.date); })
          .y(function(d) { return y(d.close); });

      // Adds the svg canvas
      var linechartsvg = d3.select("#linechartsvg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
          .append("g")
              .attr("transform",
                    "translate(" + margin.left + "," + margin.top + ")");

      // Get the data
    var data = [
        {
          "date": "1-May-12",
          "close": 58.13
        },
        {
          "date": "30-Apr-12",
          "close": 53.98
        },
        {
          "date": "27-Apr-12",
          "close": 67
        },
        {
          "date": "26-Apr-12",
          "close": 89.7
        }
      ];
          data.forEach(function(d) {
              d.date = parseDate(d.date);
              d.close = +d.close;
          });

          // Scale the range of the data
          x.domain(d3.extent(data, function(d) { return d.date; }));
          y.domain([0, d3.max(data, function(d) { return d.close; })]);

          // Add the valueline path.
          linechartsvg.append("path")
              .attr("class", "line")
              .attr("d", valueline(data));

          // Add the X Axis
          linechartsvg.append("g")
              .attr("class", "x axis")
              .attr("transform", "translate(0," + height + ")")
              .call(xAxis);

          // Add the Y Axis
          linechartsvg.append("g")
              .attr("class", "y axis")
              .call(yAxis);

    },
    componentDidMount(){
      console.log("buildLineChart component did mount");
      this.buildLineChart();
    },
    componentWillMount(){
      console.log("buildLineChart component will mount");
    },
    componentWillUnmount(){
      console.log("Line unmount");
    }
});

module.exports = SampleLineChartPanel;
